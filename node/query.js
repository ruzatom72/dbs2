var PORT = 8888

//Přidání potřebných modulů
var http = require("http");
var express = require('express');
var app = express();
var mysql = require('mysql');
var bodyParser = require('body-parser');
const { Console } = require("console");

//Spojení s DB
var con = mysql.createConnection({
  host: 'backend_database',
  database: 'data',
  port: '3306',
  user: 'root',
  password: 'Aa123456',
  insecureAuth : true
});

con.connect(function(err) {
  if (err) throw err;
  console.log("query.js Connected to database data!");
});

//Konfigurace JSON
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));



//Spuštění node.js serveru
var server = app.listen(PORT, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Api for APP listening at http://%s:%s", host, port)
});


//rest api to create a new record into mysql database
app.post('/novaData', function (req, res) {
  if (!req.body||
    !req.body.hasOwnProperty('TEPLOTA'))
    {
      !res.status(400);
      !res.send("Wrong json argument");
      console.log(req.body);
      return;
    }
  //var postDataCAS  = req.body.CAS;
  var postDataCAS = datumVeFormatu();
  var postDataTEPLOTA = req.body.TEPLOTA;

  con.query('INSERT INTO A (Cas, Teplota) VALUES (?,?)', [postDataCAS, postDataTEPLOTA], function (error, results, fields) {
   if (error) throw error;
   res.end(JSON.stringify(results));
 });
});

function datumVeFormatu(){
  return new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
}